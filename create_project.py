import pandas as pd
import numpy as np
import yaml
import sys
import fileinput
from pathlib import Path
# inject pipeline_path
pipeline_path = Path.cwd()
sys.path.insert(1, pipeline_path)

# inject pipeline_path to scripts so they can reference back to pipeline module
# This will allow each User who git clones this pipeline to have the scripts only 
# reference their copy of the pipeline. Doesn't alter the pipeline for others

def inject_pipeline_path(file):
    
    blank_pipeline_path = 'pipeline_path = \n'
    fill_pipeline_path = "pipeline_path = '{}'\n".format(Path.cwd())
    
    for line in fileinput.FileInput(file, inplace=1):
        line = line.replace(blank_pipeline_path, fill_pipeline_path)
        sys.stdout.write(line)
        
fill_pipeline_path = 'pipeline_path = {}\n'.format(Path.cwd())   
inject_pipeline_path('./helpers/create_jobs.py')
inject_pipeline_path('./helpers/generate.py')
inject_pipeline_path('./helpers/prep.py')

from helpers import prep
prep.project_dir.launch()