#!/software/R/3.5.1/bin/Rscript
rm(list=ls())

# Import libraries
library(ATACseqQC,lib.loc="/projects/b1036/SHARED/R_MODULES_atacseq")
library(MotifDb)
library(Rsamtools)
library(ChIPpeakAnno)
library(GenomicRanges)
library(BSgenome.Hsapiens.UCSC.hg19)
library(TxDb.Hsapiens.UCSC.hg19.knownGene)

# Import args
args <- commandArgs(trailingOnly = TRUE)
smpl_map_bam		= args[1]
smpl_clean_bam		= args[2]
smpl_clean_log      = args[3]
smpl_shift_bam		= args[4]
smpl_split_path		= args[5]
plot_path	        = args[6]

#--------------------------------------------------------------------------
# Part 0 - Cleaning mitochondrial reads and PCR duplicates
#--------------------------------------------------------------------------
sink(smpl_clean_log)		# this will dump R output into this file instead of terminal

bamQC(
  smpl_map_bam,
  index = smpl_map_bam,
  mitochondria = "chrM",
  outPath = smpl_clean_bam,
  doubleCheckDup = TRUE
)

#--------------------------------------------------------------------------
# Part 1 - Plot fragment distribution plot
#--------------------------------------------------------------------------
dir.create(plot_path, recursive = TRUE)	# Will output all visualizations to plot path 
setwd(plot_path)

bamfile 		<- smpl_clean_bam
bamfile.labels 	<- gsub(".bam", "", basename(bamfile))

fragSize <- fragSizeDist(bamfile, bamfile.labels)

#--------------------------------------------------------------------------
# Part 2 - Shift read start sites based on Tn5 transposase bind site
#--------------------------------------------------------------------------
tags 			<- c("AS", "XN", "XM", "XO", "XG", "NM", "MD", "YS", "YT")
#seqlev 		<- "chr1"										#--------- subset chr for quick run 
#which 			<- as(seqinfo(Hsapiens)[seqlev], "GRanges")		#--------- subset chr for quick run 
gal 			<- readBamFile(bamfile, 
					tag=tags, 
#					which=which,								#--------- subset chr for quick run 
					asMates=TRUE, 
					bigFile=TRUE)
					
gal1 			<- shiftGAlignmentsList(gal)

#----PRINT----#
print('Finished shifting bam file')

smpl_shift_bam_path <- file.path(smpl_shift_bam) 
export(gal1, smpl_shift_bam_path)

#----PRINT----#
print('Finished exporting shifted bam file')


#--------------------------------------------------------------------------
# Part 3 - Plots for nucleosome coverage
#--------------------------------------------------------------------------
txs 		<- transcripts(TxDb.Hsapiens.UCSC.hg19.knownGene)
#txs 		<- txs[seqnames(txs) %in% seqlev] 					#--------- subset chr for quick run
genome 		<- Hsapiens
objs 		<- splitGAlignmentsByCut(gal1, 
									txs=txs, 
									genome=genome)

outPath 	<- file.path(smpl_split_path)
dir.create(outPath)
null 		<- writeListOfGAlignments(objs, outPath) 
#----PRINT----#
print('split step has completed')

bamfiles 	<- file.path(outPath,
                     c("NucleosomeFree.bam",
                     "mononucleosome.bam",
                     "dinucleosome.bam",
                     "trinucleosome.bam"))
					 
TSS 		<- promoters(txs, upstream=0, downstream=1)
TSS 		<- unique(TSS)
(librarySize <- estLibSize(bamfiles)) 		# estimate the library size for normalization
NTILE 		<- 101
dws 		<- ups <- 1010
sigs 		<- enrichedFragments(gal=objs[c("NucleosomeFree", 
                                     "mononucleosome",
                                     "dinucleosome",
                                     "trinucleosome")], 
						TSS=TSS,
                        librarySize=librarySize,
#						seqlev=seqlev,					#--------- subset chr for quick run
                        TSS.filter=0.5,
                        n.tile = NTILE,
                        upstream = ups,
                        downstream = dws)
#----PRINT----#
print('finished reading in enrichedfragments')
						  
# plot heatmap of nucleosome coverage
sigs.log2 	<- lapply(sigs, function(.ele) log2(.ele+1)) # log2 transformed signals
featureAlignedHeatmap(sigs.log2, 
						reCenterPeaks(TSS, width=ups+dws),
						zeroAt=.5, 
						n.tile=NTILE)
#----PRINT----#
print('finished plotting nucleosome coverage')

# plot TSS Enrichment plot					  
out 		<- featureAlignedDistribution(sigs, 
                                  reCenterPeaks(TSS, width=ups+dws),
                                  zeroAt=.5, 
								  n.tile=NTILE, 
								  type="l", 
                                  ylab="Averaged coverage")

range01 	<- function(x){(x-min(x))/(max(x)-min(x))}
out 		<- apply(out, 2, range01)
matplot(out, type="l", xaxt="n", 
        xlab="Position (bp)", 
        ylab="Fraction of signal")
axis(1, at=seq(0, 100, by=10)+1, 
     labels=c("-1K", seq(-800, 800, by=200), "1K"), las=2)
abline(v=seq(0, 100, by=10)+1, lty=2, col="gray")

#----PRINT----#
print('finished plotting tss enrichment plot')

#--------------------------------------------------------------------------
# Part 4 - Plot TF footprint
#--------------------------------------------------------------------------
CTCF 		<- query(MotifDb, c("CTCF"))
CTCF 		<- as.list(CTCF)
sigs 		<- factorFootprints(smpl_shift_bam_path, 
						pfm=CTCF[[1]], 
                        genome=genome,
                        min.score="90%", 
#						seqlev=seqlev,						#--------- subset chr for quick run
                        upstream=100, downstream=100)
#----PRINT----#
print('finished plotting TF footprint')