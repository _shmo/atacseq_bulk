import pandas as pd
from pathlib import Path
import argparse
#
import sys
sys.path.insert(1, '/projects/b1036/shmo/atacseq')
from helpers import prep


project_path,\
fastqc_path,\
trim_path,\
fastqc_trim_path,\
map_path,\
filter_chrM_path,\
filter_pcr_path,\
sort_index_path, \
shift_path, \
bed_path,\
peak_call_path, \
visualize_path = prep.project_dir.acquire_info()


parser = argparse.ArgumentParser(description='Read counts to calculate chrM percentage')
parser.add_argument('smpl', type=str, help='Sample to calculate chrM percentage')
args = parser.parse_args()


def write_chrM_pct(smpl):
    smpl_map_bam = '{0}/{1}.map.bam'.format(map_path, smpl)
    smpl_map_count = '{0}/{1}.map.count'.format(map_path, smpl)

    smpl_filter_chrM_bam = '{0}/{1}.filter_chrM.bam'.format(filter_chrM_path, smpl)
    smpl_filter_chrM_count = '{0}/{1}.filter_chrM.count'.format(filter_chrM_path, smpl)

    total_reads = int(pd.read_csv(Path(smpl_map_count)).columns[0])
    filter_chrM_reads = int(pd.read_csv(Path(smpl_filter_chrM_count)).columns[0])
    chrM_percentage = 100 * (total_reads - filter_chrM_reads) / total_reads

    chrM_pct_report = '{}.chrM.pct'.format(smpl)
    chrM_pct_report_path = filter_chrM_path / chrM_pct_report

    with open(chrM_pct_report_path, 'w') as report:
        
        report.write('Report for sample: {}\n'.format(smpl))
        report.write('Percentage of Mitochondrial reads: {0:.2f} %\n'.format(chrM_percentage))
        report.write('Number of total mapped reads: {}\n'.format(total_reads))
        report.write('Number of total mitochondrial reads: {}'.format(filter_chrM_reads))
        
        
write_chrM_pct(args.smpl)