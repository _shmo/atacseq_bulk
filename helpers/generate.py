import yaml
import re
import sys
import pandas as pd
from pathlib import Path
# import scripts
# pipeline_path will get populated first time run create_project.py
pipeline_path = 
sys.path.insert(1, pipeline_path)
from helpers import prep, readin


# Define directories in project directory
project_path,\
fastqc_path,\
trim_path,\
map_path,\
clean_path,\
shift_path,\
split_path, \
plot_path, \
peak_path = prep.project_dir.acquire_info()


# Define paths to necessary elements
path_local_yaml_stream = open(Path('{}/config/path_local.yml'.format(pipeline_path)), 'r')
path_quest_yaml_stream = open(Path('{}/config/path_quest.yml'.format(pipeline_path)), 'r')
path_local = yaml.load(path_local_yaml_stream)
path_quest = yaml.load(path_quest_yaml_stream)


# create job directory
job_dir = Path('{}/_jobs'.format(project_path))
job_dir.mkdir(exist_ok=True)


def write_run_all_script(sub_dir):
    
    run_all_script = 'RUN_ALL.sh'
    run_all_path = sub_dir / run_all_script
    with open(run_all_path, 'w') as run_all:
        run_all.write('')
        
    return run_all_path
    

def fastqc():

    fastqc_dir = job_dir / '1_fastqc'
    fastqc_dir.mkdir(exist_ok=True)
        
    run_all_path = write_run_all_script(fastqc_dir)

    ### JOB TEXT ###
    b1042_template =  '{}/'.format(pipeline_path) + path_local['header']['b1042_template']
    fastqc_template = '{}/'.format(pipeline_path) + path_local['fastqc']['fastqc_template']

    # iterathrough through each sample in input_fastq.yml and create jobs
    input_fastq = readin.fastq(project_path)
    for smpl in input_fastq:

        # define sample names
        smpl_R1 = input_fastq[smpl][0]
        smpl_R2 = input_fastq[smpl][1]

        # for each sample, write a job 
        fastqc_job_script = 'fastqc_{}.sh'.format(smpl)
        fastqc_job_path = fastqc_dir / fastqc_job_script

        with open(fastqc_job_path, 'w') as job, \
        open(b1042_template, 'r') as header_text, \
        open(fastqc_template, 'r') as fastqc_text:

            # write header text
            header_text_read = header_text.readlines()
            job.writelines(header_text_read)
            job.write('\n#SBATCH -J fastqc_{}\n\n'.format(smpl))

            # write variable exports
            job.write('export smpl_R1={}\n'.format(smpl_R1))
            job.write('export smpl_R2={}\n'.format(smpl_R2))
            job.write('export fastqc_path={}\n'.format(fastqc_path))

            # write fastqc text
            fastqc_text_read = fastqc_text.readlines()
            job.writelines(fastqc_text_read)

        with open(run_all_path, 'a') as run_all:
            run_all.write('sbatch {}\n'.format(fastqc_job_path))
            
            
def trim():

    trim_dir = job_dir / '2_trim'
    trim_dir.mkdir(exist_ok=True)
    
    run_all_path = write_run_all_script(trim_dir)

    ### JOB TEXT ###
    b1042_template = '{}/'.format(pipeline_path) + path_local['header']['b1042_template']
    trim_template = '{}/'.format(pipeline_path) + path_local['trim']['trim_template']

    # iterathrough through each sample in input_fastq.yml and create jobs
    input_fastq = readin.fastq(project_path)
    for smpl in input_fastq:

        # define sample names
        smpl_R1 = input_fastq[smpl][0]
        smpl_R2 = input_fastq[smpl][1]
        
        # define job export variables
        smpl_R1_P_trim = '{0}/{1}_R1_P.trim.fastq'.format(trim_path,smpl)
        smpl_R1_U_trim = '{0}/{1}_R1_U.trim.fastq'.format(trim_path,smpl)
        smpl_R2_P_trim = '{0}/{1}_R2_P.trim.fastq'.format(trim_path,smpl)
        smpl_R2_U_trim = '{0}/{1}_R2_U.trim.fastq'.format(trim_path,smpl)
        trimmomatic = '{}/'.format(pipeline_path) + path_local['trim']['trimmomatic']
        adapter_fa = '{}/'.format(pipeline_path) + path_local['trim']['adapter_fa'] 
        
        job_vars = ['smpl_R1', 'smpl_R2', 'smpl_R1_P_trim', 'smpl_R1_U_trim', 'smpl_R2_P_trim', 'smpl_R2_U_trim', 
                    'trimmomatic', 'adapter_fa']

        # for each sample, write a job 
        trim_job_script = 'trim_{}.sh'.format(smpl)
        trim_job_path = trim_dir / trim_job_script

        with open(trim_job_path, 'w') as job, \
        open(b1042_template, 'r') as header_text, \
        open(trim_template, 'r') as trim_text:

            # write header text
            header_text_read = header_text.readlines()
            job.writelines(header_text_read)
            job.write('\n#SBATCH -J trim_{}\n\n'.format(smpl))
            
            # write export variables
            for job_var in job_vars:
                job.write('export {}={}\n'.format(job_var, locals()[job_var]))
            job.write('export fastqc_path={}\n'.format(fastqc_path))

            # write trim text
            trim_text_read = trim_text.readlines()
            job.writelines(trim_text_read)

        with open(run_all_path, 'a') as run_all:
            run_all.write('sbatch {}\n'.format(trim_job_path))
            
            
def map_to_ref():

    map_dir = job_dir / '3_map'
    map_dir.mkdir(exist_ok=True)

    run_all_path = write_run_all_script(map_dir)
    
    ### JOB TEXT ###
    b1042_template = '{}/'.format(pipeline_path) + path_local['header']['b1042_template']
    map_template = '{}/'.format(pipeline_path) + path_local['map']['map_template']
    hg19_ref = path_quest['map']['hg19_ref']

    # iterathrough through each sample in input_fastq.yml and create jobs
    input_fastq = readin.fastq(project_path)
    for smpl in input_fastq:
        
        # define job export variables
        smpl_R1_P_trim = '{0}/{1}_R1_P.trim.fastq'.format(trim_path,smpl)
        smpl_R2_P_trim = '{0}/{1}_R2_P.trim.fastq'.format(trim_path,smpl)
        smpl_map_bam = '{0}/{1}.map.bam'.format(map_path, smpl)
        job_vars = ['hg19_ref', 'smpl_R1_P_trim', 'smpl_R2_P_trim', 'smpl_map_bam']

        # for each sample, write a job 
        map_job_script = 'map_{}.sh'.format(smpl)
        map_job_path = map_dir / map_job_script

        with open(map_job_path, 'w') as job, \
        open(b1042_template, 'r') as header_text, \
        open(map_template, 'r') as map_text:

            # write header text
            header_text_read = header_text.readlines()
            job.writelines(header_text_read)
            job.write('\n#SBATCH -J map_{}\n\n'.format(smpl))

            # write variable exports
            for job_var in job_vars:
                job.write('export {}={}\n'.format(job_var, locals()[job_var]))

            # write map text
            map_text_read = map_text.readlines()
            job.writelines(map_text_read)

        with open(run_all_path, 'a') as run_all:
            run_all.write('sbatch {}\n'.format(map_job_path))
            
            
def ATACseqQC():

    ATACseqQC_dir = job_dir / '4_ATACseqQC'
    ATACseqQC_dir.mkdir(exist_ok=True)

    run_all_path = write_run_all_script(ATACseqQC_dir)

    ### JOB TEXT ###
    b1042_template = '{}/'.format(pipeline_path) + path_local['header']['b1042_template']
    ATACseqQC_template = '{}/'.format(pipeline_path) + path_local['ATACseqQC']['ATACseqQC_template']

    # iterate through through each sample in input_fastq.yml and create jobs
    input_fastq = readin.fastq(project_path)
    for smpl in input_fastq:
        
        # define job export variables
        ATACseqQC_script = '{}/'.format(pipeline_path) + path_local['ATACseqQC']['ATACseqQC_script']
        smpl_map_bam = '{0}/{1}.map.bam'.format(map_path, smpl)
        smpl_clean_bam = '{0}/{1}.clean.bam'.format(clean_path, smpl)
        smpl_clean_log = '{0}/{1}.clean.log'.format(clean_path, smpl)
        smpl_shift_bam = '{0}/{1}.shift.bam'.format(shift_path, smpl)
        smpl_split_path = '{0}/{1}/'.format(split_path, smpl)
        smpl_plot_path = '{0}/{1}/'.format(plot_path, smpl)
        job_vars = ['ATACseqQC_script', 'smpl_map_bam', 'smpl_clean_bam', 'smpl_clean_log',
                    'smpl_shift_bam', 'smpl_split_path', 'smpl_plot_path']

        # for each sample, write a job 
        ATACseqQC_job_script = 'ATACseqQC_{}.sh'.format(smpl)
        ATACseqQC_job_path = ATACseqQC_dir / ATACseqQC_job_script

        with open(ATACseqQC_job_path, 'w') as job, \
        open(b1042_template, 'r') as header_text, \
        open(ATACseqQC_template, 'r') as ATACseqQC_text:

            # write header text
            header_text_read = header_text.readlines()
            job.writelines(header_text_read)
            job.write('\n#SBATCH -J ATACseqQC_{}\n\n'.format(smpl))

            # write export variables
            for job_var in job_vars:
                job.write('export {}={}\n'.format(job_var, locals()[job_var]))

            # write visualize text
            ATACseqQC_text_read = ATACseqQC_text.readlines()
            job.writelines(ATACseqQC_text_read)

        with open(run_all_path, 'a') as run_all:
            run_all.write('sbatch {}\n'.format(ATACseqQC_job_path))
            
            
def peak_call():

    peak_call_dir = job_dir / '5_peak'
    peak_call_dir.mkdir(exist_ok=True)
    
    run_all_path = write_run_all_script(peak_call_dir)

    ### JOB TEXT ###
    b1042_template = '{}/'.format(pipeline_path) + path_local['header']['b1042_template']
    peak_call_template = '{}/'.format(pipeline_path) + path_local['peak_call']['peak_call_template']

    # iterathrough through each sample in input_fastq.yml and create jobs
    input_fastq = readin.fastq(project_path)
    for smpl in input_fastq:
        
        # define job export variables
        smpl_shift_bam = '{0}/{1}.shift.bam'.format(shift_path, smpl)

        # for each sample, write a job 
        peak_call_job_script = 'peak_{}.sh'.format(smpl)
        peak_call_job_path = peak_call_dir / peak_call_job_script

        with open(peak_call_job_path, 'w') as job, \
        open(b1042_template, 'r') as header_text, \
        open(peak_call_template, 'r') as peak_call_text:

            # write header text
            header_text_read = header_text.readlines()
            job.writelines(header_text_read)
            job.write('\n#SBATCH -J peak_call_{}\n\n'.format(smpl))

            # write variable exports
            job.write('export smpl={}\n'.format(smpl))
            job.write('export smpl_shift_bam={}\n'.format(smpl_shift_bam))
            job.write('export peak_path={}\n'.format(peak_path))

            # write peak_call text
            peak_call_text_read = peak_call_text.readlines()
            job.writelines(peak_call_text_read)

        with open(run_all_path, 'a') as run_all:
            run_all.write('sbatch {}\n'.format(peak_call_job_path))