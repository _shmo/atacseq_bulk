import numpy as np
import pandas as pd
import yaml
import shutil
import datetime
from pathlib import Path

pipeline_path = 
atacseq_dir = Path(pipeline_path)


class project_dir:

    @staticmethod
    def launch():
        
        # read local project.yml file and create project directory
        project_yaml_stream = open(atacseq_dir / '_project.yml', 'r')
        project_yaml = yaml.load(project_yaml_stream)
        project_name = project_yaml['project']['name']
        project_path = Path(project_yaml['project']['path']) / '__atacseq__{}'.format(project_name)
        project_path.mkdir(exist_ok=True)
        
        # copy _project.yml to the config file
        transfer_file = Path(pipeline_path) / '_project.yml'
        dup_path = Path(pipeline_path) / 'config'
        shutil.copy2(transfer_file, dup_path)
        
        # copy over the config folder to the project dir
        transfer_config = Path(pipeline_path) / 'config/'
        dup_path = project_path / '_config'
        try:
            shutil.copytree(transfer_config, dup_path)
        except FileExistsError:
            print('The config dir in this project dir already exists.')
            
        # copy over create_job.py script to project dir
        transfer_file = atacseq_dir / 'helpers/create_jobs.py'
        shutil.copy2(transfer_file, project_path)
        
        # create output folders in directory
        fastqc_path = project_path / '1_fastqc'
        fastqc_path.mkdir(exist_ok=True)
        
        trim_path = project_path / '2_trim'
        trim_path.mkdir(exist_ok=True)
        
        map_path = project_path / '3_map'
        map_path.mkdir(exist_ok=True)
        
        clean_path = project_path / '4_clean'
        clean_path.mkdir(exist_ok=True)
        
        shift_path = project_path / '5_shift'
        shift_path.mkdir(exist_ok=True)
        
        split_path = project_path / '6_split'
        split_path.mkdir(exist_ok=True)
        
        plot_path = project_path / '7_plot'
        plot_path.mkdir(exist_ok=True)
        
        peak_path = project_path / '8_peak'
        peak_path.mkdir(exist_ok=True)
        
        
    @staticmethod
    def acquire_info():
        
        # read local project.yml file and create project directory
        # ensure that correct project information is in project.yml when running create_job.py
        project_yaml_stream = open('./_config/_project.yml', 'r')
        project_yaml = yaml.load(project_yaml_stream)
        project_name = project_yaml['project']['name']
        project_path = Path(project_yaml['project']['path']) / '__atacseq__{}'.format(project_name)
        
        # acquire name of output paths
        fastqc_path = project_path / '1_fastqc'
        trim_path = project_path / '2_trim'
        map_path = project_path / '3_map'
        clean_path = project_path / '4_clean'
        shift_path = project_path / '5_shift'
        split_path = project_path / '6_split'
        plot_path = project_path / '7_plot'
        peak_path = project_path / '8_peak'
        
        return project_path, fastqc_path, trim_path, map_path, clean_path, shift_path, split_path, plot_path, peak_path