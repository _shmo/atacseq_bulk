# import std libraries
import pandas as pd
import numpy as np
import yaml
import math
import sys
from pathlib import Path
# import scripts
pipeline_path = 
sys.path.insert(1, pipeline_path)
from helpers import prep, readin, generate


# Generate the job scripts for each process
generate.fastqc()
generate.trim()
generate.map_to_ref()
generate.ATACseqQC()
generate.peak_call()