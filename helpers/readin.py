import yaml
from pathlib import Path


def fastq(project_path):
    
    config_folder = project_path / '_config'
    input_fastq_stream = open(Path(config_folder / 'input_fastq.yml'), 'r')
    input_fastq = yaml.load(input_fastq_stream)
    
    return input_fastq